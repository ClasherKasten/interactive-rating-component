module.exports = {
  content: ["./src/**/*.{html,js,css}"],
  theme: {
    extend: {
			fontFamily: {
				"overpass": ["Overpass"]
			},
			colors: {
				"rc-orange": "hsl(25, 97%, 53%)",
				"rc-white": "hsl(0, 0%, 100%)",
				"rc-light-grey": "hsl(217, 12%, 63%)",
				"rc-medium-grey": "hsl(216, 12%, 23%)",
				"rc-blue": "hsl(213, 19%, 18%)",
				"rc-dark-blue": "hsl(216, 12%, 8%)"
			}
		},
  },
  plugins: [],
}
