var rating_buttons = document.getElementsByClassName('rating-button');
let submit_button = document.getElementById('submit-button');
let rating_box = document.getElementById('rating-box');
let thank_you_box = document.getElementById('thank-you-box');
let selected_label = document.getElementById('ty-selected-label');

function getRating() {
	for (const rating_button of rating_buttons) {
		if (rating_button.classList.contains('rating-button--active')) {
			return rating_button.innerText;
		}
	}
	return ''
}

thank_you_box.style.display = 'none';

submit_button.addEventListener('click', (e) => {
	rating_box.style.display = 'none';
	thank_you_box.style.display = 'flex';
	selected_label.innerText = getRating();
});

for (const rating_button of rating_buttons) {
	rating_button.addEventListener('click', (e) => {
		for (const rating_button of rating_buttons) {
			rating_button.classList.remove('rating-button--active');
		}
		e.target.classList.add('rating-button--active');
	});
}